### Theme

In this section we will start exploring Git with GitLab. Please ensure you have already downloaded [VSC](https://code.visualstudio.com/)

### Key Tasks to Complete

# Step 0: Install Git

1. If you already have Git installed feel free to relax for a minute, but for those that do not determine your computers OS then follow the steps below based on your OS:

    - **Windows**: Go to https://git-scm.com/download/win and the download should start. Reference: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
    - **MacOS**: Should already be installed, run _git --version_ to confirm
    - **Linux**: For Fedora, RHEL, or CentOS run _sudo dnf install git-all_, then run _sudo dnf install getopt_, and finally run _sudo ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi_. For Ubuntu run _sudo apt install git-all_ then run _sudo apt-get install install-info_

# Step 1: Set Up Local Development

1. Now that we have everything installed we can get started by cloning our project locally to set up our workspace. Navigate to the main view of the Tanuki Racing project that was imported into our group, then click **Code** on the right hand side.

2. From here we are going to want to click the **copy** icon in the **_Clone with HTTPS_** section and copy the resulting url locally for now. We will need this in a few steps.

3. Next we are going to set up our personal access token to be able to bring our projects down from GitLab. In the top left of the GitLab view click your profile picture then open your preferences in a new tab.

4. On the resulting setting page we will want to use the left hand navigation menu to click Access Tokens. Click **Add new token**

5. Name the token _VSC_, set an expiration date, then check _api, read_api, read_user, read_repository, write_repository, read_registry, write_registry, and ai_features_. We are selecting so many options as this token will let us take advantage of the full GitLab VSC integration.

6. Lastly click **Create personal access token** then copy the resulting token locally.

# Step 1: Configuring VSC

1. Navigate to VSC and use the left hand navigation menu to click **Extensions**. From here in the extension search bar we will type _GitLab Workflow_ and click **Install**

2. Once installed we are first going to want to click the **Settings icon** then **Extension Settings** on the dropdown. From here we want to ensure **GitLab Duo Code Suggestions** is checked.

3. Next click **Command + Shift + P**, then in the search bar input _GitLab: Add Account_ and hit _Enter_. When prompted for the GitLab instance use _https://gitlab.com/_, then in the next prompt input the access token we copied locally earlier and hit enter.

4. Next we will go ahead and click **Terminal>New Terminal** on our toolbar. Within our new terminal we will run the below Git command:

    ```
    git clone https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing.git
    ```

    > If you get a collision with the folder name you can add your target folder name to the end of the command

5. We then are going to use the left hand navigation menu to click **Explorer**, **Open Folder** and open _Tanuki Racing_.

6. We now have git set up to start development on _Tanuki Racing_! We will first plan out some of our changes before moving forward.